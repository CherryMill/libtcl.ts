var chai = require('chai');

let tcl = require("../../dist/libtcl.min");

// TC-push: Tests for feature 'tcl.push'
describe(
	'[TC-push-1]',
	() => it('push should be able to be invoked as a function',
	() => chai.expect(tcl.push).to.be.a('function'))
);

describe(
	'[TC-pop-1]',
	() => it('pop should be able to be invoked as a function',
	() => chai.expect(tcl.pop).to.be.a('function'))
);