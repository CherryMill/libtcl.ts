var chai = require("chai");

let tcl = require("../../dist/libtcl.min");

// TC-range: Tests for feature 'tcl.range'
describe("[TC-range-1]", () =>
  it("range should be able to be invoked as a function", () =>
    chai.expect(tcl.range).to.be.a("function")));

describe("[TC-range-2]", () =>
  it("tcl.range normal test cases", () => {
    return (
      chai.expect(tcl.range(1, 2, 3).toString()).to.equal([1].toString()) &&
      chai.expect(tcl.range(1, 6).toString()).to.equal([1, 2, 3, 4, 5].toString()) &&
      chai.expect(tcl.range(1, 6, 2).toString()).to.equal([1, 3, 5].toString()) &&
      chai.expect(tcl.range(6, 1).toString()).to.equal([6, 5, 4, 3, 2].toString()) &&
      chai.expect(tcl.range(6, 1, -2).toString()).to.equal([6, 4, 2].toString()) &&
      chai.expect(tcl.range(6).toString()).to.equal([0, 1, 2, 3, 4, 5].toString()) &&
      chai.expect(tcl.range(-6).toString()).to.equal([-6, -5, -4, -3, -2, -1].toString())
    );
  }));

// TC-chunk: Tests for feature 'tcl.chunk'
describe("[TC-chunk-1]", () =>
  it("chunk should be able to be invoked as a function", () =>
    chai.expect(tcl.chunk).to.be.a("function")));

describe("[TC-chunk-2]", () =>
  it("tcl.chunk normal test cases", () => {
    return (
      chai.expect(tcl.chunk([1, 2, 3, 4, 5, 6, 7]).toString()).to.equal([1, 2, 3, 4, 5, 6, 7].toString()) &&
      chai.expect(tcl.chunk([1, 2, 3, 4, 5, 6, 7], 2).length).to.equal(4) &&
      chai.expect(tcl.chunk([1, 2, 3, 4, 5, 6, 7], 3).length).to.equal(3) &&
      chai.expect(tcl.chunk([1, 2, 3, 4, 5, 6, 7], 4).length).to.equal(2) &&
      chai.expect(tcl.chunk([1, 2, 3, 4, 5, 6, 7], -1).length).to.equal(1) &&
      chai.expect(tcl.chunk([1, 2, 3, 4, 5, 6, 7], 'x').length).to.equal(1)
    );
  }));

// TC-object: Tests for feature 'tcl.object'
describe("[TC-object-1]", () =>
  it("object should be able to be invoked as a function", () =>
    chai.expect(tcl.object).to.be.a("function")));

describe("[TC-object-2]", () =>
  it("tcl.object normal test cases", () => {
    return (
      chai.expect(JSON.stringify(tcl.object(['a', 'b'], [1,2]))).to.equal(JSON.stringify({'a': 1, 'b': 2})) &&
      chai.expect(JSON.stringify(tcl.object(['a', 'b'], [1]))).to.equal(JSON.stringify({'a': 1})) &&
      chai.expect(JSON.stringify(tcl.object(['a'], [1,2]))).to.equal(JSON.stringify({'a': 1})) &&
      chai.expect(JSON.stringify(tcl.object('x'))).to.equal(JSON.stringify({})) &&
      chai.expect(JSON.stringify(tcl.object('x', 1))).to.equal(JSON.stringify({'x': 1}))
    );
  }));