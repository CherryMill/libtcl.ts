var chai = require("chai");

let tcl = require("../../dist/libtcl.min");

// TC-union: Tests for feature 'tcl.union'
describe("[TC-union-1]", () =>
  it("union should be able to be invoked as a function", () =>
    chai.expect(tcl.union).to.be.a("function")));

describe("[TC-union-2]", () =>
  it("tcl.union normal test cases", () => {
    return (
      chai.expect(tcl.union([1, 2, 3]).toString()).to.equal([1, 2, 3].toString()) &&
      chai.expect(tcl.union([]).toString()).to.equal([].toString()) &&
      chai.expect(tcl.union([1, 2, 3], -1).toString()).to.equal([1, 2, 3, -1].toString()) &&
      chai.expect(tcl.union([1, 2, 3], "xyz").toString()).to.equal([1, 2, 3, "xyz"].toString()) &&
      chai.expect(tcl.union([1, 2, 3], []).toString()).to.equal([1, 2, 3].toString()) &&
      chai.expect(tcl.union([1, 2, 3], [2, 3, 1], [-1]).toString()).to.equal([1, 2, 3, -1].toString()) &&
      chai.expect(tcl.union([1, 2, 3, 4], [1, 2]).toString()).to.equal([1, 2, 3, 4].toString())
    );
  }));

// TC-intersection: Tests for feature 'tcl.intersection'
describe("[TC-intersection-1]", () =>
  it("intersection should be able to be invoked as a function", () =>
    chai.expect(tcl.intersection).to.be.a("function")));

describe("[TC-intersection-2]", () =>
  it("tcl.intersection normal test cases", () => {
    return (
      chai.expect(tcl.intersection([1, 2, 3]).toString()).to.equal([1, 2, 3].toString()) &&
      chai.expect(tcl.intersection([1, 2, 3], [1, 2, 3, 4], [3, 2, 1]).toString()).to.equal([1, 2, 3].toString()) &&
      chai.expect(tcl.intersection([1, 2, 3], [1]).toString()).to.equal([1].toString()) &&
      chai.expect(tcl.intersection([1], [1, 2, 3]).toString()).to.equal([1].toString()) &&
      chai.expect(tcl.intersection([], [1, 2, 3]).toString()).to.equal([].toString())
    );
  }));

// TC-difference: Tests for feature 'tcl.difference'
describe("[TC-difference-1]", () =>
  it("difference should be able to be invoked as a function", () =>
    chai.expect(tcl.difference).to.be.a("function")));

describe("[TC-difference-2]", () =>
  it("tcl.difference normal test cases", () => {
    return (
      chai.expect(tcl.difference([1, 2, 3], [2]).toString()).to.equal([1, 3].toString()) &&
      chai.expect(tcl.difference([1, 2, 3], [1, 2, 3, 4]).toString()).to.equal([].toString()) &&
      chai.expect(tcl.difference([1, 2, 3], [1]).toString()).to.equal([2, 3].toString()) &&
      chai.expect(tcl.difference([1], [1, 2, 3]).toString()).to.equal([].toString()) &&
      chai.expect(tcl.difference([], [1, 2, 3]).toString()).to.equal([].toString())
    );
  }));