var chai = require("chai");

let tcl = require("../../dist/libtcl.min");

// TC-rest: Tests for feature 'tcl.rest'
describe("[TC-rest-1]", () =>
  it("rest should be able to be invoked as a function", () =>
    chai.expect(tcl.rest).to.be.a("function")));

describe("[TC-rest-2]", () =>
  it("tcl.rest normal test cases", () => {
    return (
      chai.expect(tcl.rest([1, 2, 3]).toString()).to.equal([2,3].toString()) &&
      chai.expect(tcl.rest([]).toString()).to.equal([].toString()) &&
      chai.expect(tcl.rest([1, 2, 3], -1).toString()).to.equal([1, 2, 3].toString()) &&
      chai.expect(tcl.rest([1, 2, 3], "xyz").toString()).to.equal([1,2,3].toString()) &&
      chai.expect(tcl.rest("xyz", -2)).to.equal("xyz") &&
      chai
        .expect(tcl.rest([1, 2, 3], 1).toString())
        .to.equal([2, 3].toString()) &&
      chai
        .expect(tcl.rest([1, 2, 3], 4).toString())
        .to.equal([].toString())
    );
  }));

  // TC-flatten: Tests for feature 'tcl.flatten'
describe("[TC-flatten-1]", () =>
it("flatten should be able to be invoked as a function", () =>
  chai.expect(tcl.flatten).to.be.a("function")));

describe("[TC-flatten-2]", () =>
it("tcl.flatten normal test cases", () => {
  return (
    chai.expect(tcl.flatten([1, 2, 3]).toString()).to.equal([1,2,3].toString()) &&
    chai.expect(tcl.flatten([]).toString()).to.equal([].toString()) &&
    chai.expect(tcl.flatten([1, 2, [[[3]]]]).toString()).to.equal([1, 2, 3].toString()) &&
    chai.expect(tcl.flatten([1, [[2,3],3], 3]).toString()).to.equal([1,2,3,3,3].toString()) &&
    chai.expect(tcl.flatten("xyz")).to.equal("xyz")
  );
}));

// TC-compact: Tests for feature 'tcl.compact'
describe("[TC-compact-1]", () =>
  it("compact should be able to be invoked as a function", () =>
    chai.expect(tcl.compact).to.be.a("function")));

describe("[TC-compact-2]", () =>
  it("tcl.compact normal test cases", () => {
    return (
      chai.expect(tcl.compact([1, 2, 3, [], '']).toString()).to.equal([1,2,3, []].toString()) &&
      chai.expect(tcl.compact([]).toString()).to.equal([].toString()) &&
      chai.expect(tcl.compact([1, 2, false,[[[3]]]]).toString()).to.equal([1, 2, 3].toString()) &&
      chai.expect(tcl.compact("xyz")).to.equal("xyz")
    );
  }));

  // TC-without: Tests for feature 'tcl.without'
describe("[TC-without-1]", () =>
it("without should be able to be invoked as a function", () =>
  chai.expect(tcl.without).to.be.a("function")));

describe("[TC-without-2]", () =>
it("tcl.without normal test cases", () => {
  return (
    chai.expect(tcl.without([1, 2, 3],2,3,4).toString()).to.equal([1].toString()) &&
    chai.expect(tcl.without([]).toString()).to.equal([].toString()) &&
    chai.expect(tcl.without([1, 2, 3,]).toString()).to.equal([1, 2, 3].toString()) &&
    chai.expect(tcl.without([1, 2], 1, 2).toString()).to.equal([].toString()) &&
    chai.expect(tcl.without("xyz")).to.equal("xyz")
  );
}));