var chai = require("chai");

let tcl = require("../../dist/libtcl.min");

// TC-first: Tests for feature 'tcl.first'
describe("[TC-first-1]", () =>
  it("first should be able to be invoked as a function", () =>
    chai.expect(tcl.first).to.be.a("function")));

describe("[TC-first-2]", () =>
  it("tcl.first normal test cases", () => {
    return (
      chai.expect(tcl.first([1, 2, 3])).to.equal(1) &&
      chai.expect(tcl.first([])).to.equal(null) &&
      chai.expect(tcl.first([1, 2, 3], -1)).to.equal(null) &&
      chai.expect(tcl.first([1, 2, 3], "xyz")).to.equal(null) &&
      chai.expect(tcl.first("xyz", -2)).to.equal(null) &&
      chai.expect(tcl.first({n: 1})).to.equal(null) &&
      chai
        .expect(tcl.first([1, 2, 3], 1).toString())
        .to.equal([1].toString()) &&
      chai
        .expect(tcl.first([1, 2, 3], 3).toString())
        .to.equal([1, 2, 3].toString())
    );
  }));

// TC-reverse: Tests for feature 'tcl.reverse'
describe("[TC-reverse-1]", () =>
  it("reverse should be able to be invoked as a function", () =>
    chai.expect(tcl.reverse).to.be.a("function")));

describe("[TC-reverse-2]", () =>
  it("tcl.reverse normal test cases", () => {
    return (
      chai
        .expect(tcl.reverse([1, 2, 3]).toString())
        .to.equal([3,2,1].toString()) &&
        chai
        .expect(tcl.reverse('xyv').toString())
        .to.equal('xyv') &&
        chai
        .expect(tcl.reverse([1]).toString())
        .to.equal([1].toString()) &&
      chai
        .expect(tcl.reverse([]).toString())
        .to.equal([].toString())
    );
  }));

// TC-initial: Tests for feature 'tcl.initial'
describe("[TC-initial-1]", () =>
  it("initial should be able to be invoked as a function", () =>
    chai.expect(tcl.initial).to.be.a("function")));

describe("[TC-initial-2]", () =>
  it("tcl.initial normal test cases", () => {
    return (
      chai
        .expect(tcl.initial([1, 2, 3]).toString())
        .to.equal([1,2].toString()) &&
        chai
        .expect(tcl.initial('xyv').toString())
        .to.equal('xyv') &&
        chai
        .expect(tcl.initial([1]).toString())
        .to.equal([].toString()) &&
      chai
        .expect(tcl.initial([]).toString())
        .to.equal([].toString())
    );
  }));

// TC-last: Tests for feature 'tcl.last'
describe("[TC-last-1]", () =>
  it("last should be able to be invoked as a function", () =>
    chai.expect(tcl.last).to.be.a("function")));

describe("[TC-last-2]", () =>
  it("tcl.last normal test cases", () => {
    return (
      chai
        .expect(tcl.last([1, 2, 3]).toString())
        .to.equal('3') &&
        chai
        .expect(tcl.last('xyv').toString())
        .to.equal('xyv') &&
        chai
        .expect(tcl.last([1]).toString())
        .to.equal('1') &&
      chai
        .expect(tcl.last([]).toString())
        .to.equal([].toString())
    );
  }));