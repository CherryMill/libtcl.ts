var chai = require('chai');

let tcl = require("../dist/libtcl.min");
require('./Features/index')

// TC-tcl: Generic Tests for Object 'tcl'
describe(
	'[TC-tcl-1]',
	() => it('tcl should be able to imported',
	() => chai.expect(tcl).to.be.an('object'))
);

describe(
	'[TC-tcl-2]',
	() => it('tcl should be able to invoke itself via tcl.self',
	() => chai.expect(tcl.self).to.equal(tcl))
);