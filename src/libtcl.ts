((): void => {
  // --- Function Helpers

  // --- Obtain global object
  // TBD: Currently, this script supports Node.js only
  let oRoot: object = global;
  if ((typeof oRoot).toString() === 'undefined') {
    return;
  }
  // ---

  // --- Mount tcl object to global execution context
  //     and export object to node
  let tcl: object = {};

  /**
   * @function callback
   * @param cbFn    Callabck function to evaluate
   * @param arg     Argument for function cbFn
   */
  let callback = (cbFn: (...element: any[]) => any, ...arg: any[]): any => {
    return cbFn.call(null, ...arg);
  };

  // --- Object Functions
  /**
   * @function isUndefined
   * @param suspect Suspect for checking
   */
  let isUndefined = (suspect: any): boolean => {
    return typeof suspect === 'undefined';
  };

  /**
   * @function isArray
   * @param suspect Suspect for checking
   */
  let isArray = (suspect: any): boolean => {
    return Array.isArray(suspect);
  };

  /**
   * @function isObject
   * @param suspect Suspect for checking
   */
  let isObject = (suspect: any): boolean => {
    return typeof suspect === 'object';
  };

  /**
   * @function isInteger
   * @param suspect Suspect for checking
   */
  let isInteger = (suspect: any): boolean => {
    return Number.isInteger(suspect);
  };

  /**
   * @function isNumberLike
   * @param suspect Suspect for checking
   * 
   * @description This function checks whether a suspect is
   * a number or is able to be converted into number
   */
  let isNumberLike = (suspect: any): boolean => {
    return !Number.isNaN(Number(suspect));
  };

  /**
   * @function isNonNegativeInteger
   * @param suspect Suspect for checking
   */
  let isNonNegativeInteger = (suspect: any): boolean => {
    return !!callback(isInteger, suspect) && suspect >= 0;
  };

  /**
   * @function isNull
   * @param suspect Suspect for checking
   */
  let isNull = (suspect: any): boolean => {
    return suspect === null;
  };

  let safeGetPrototypeString = (suspect: any): string => {
    if (callback(isNull, suspect)) {
      return null;
    } else if (callback(isUndefined, suspect)) {
      return undefined;
    } else {
      return Object.getPrototypeOf(suspect).toString();
    }
  };

  /**
   * @function isNaN
   * @param suspect Suspect for checking
   * 
   * @description Native method
   */
  let isNaN = (suspect: any): boolean => {
    return Number.isNaN(suspect);
  };

  /**
   * @function isWeakSet
   * @param suspect Suspect for checking
   */
  let isWeakSet = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/[ ]+WeakSet[ ]*/i);
  };

  /**
   * @function isSet
   * @param suspect Suspect for checking
   */
  let isSet = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/[ ]+Set[ ]*/i);
  };

  /**
   * @function isWeakSet
   * @param suspect Suspect for checking
   */
  let isWeakMap = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/[ ]+WeakMap[ ]*/i);
  };

  /**
   * @function isSet
   * @param suspect Suspect for checking
   */
  let isMap = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/[ ]+Map[ ]*/i);
  };

  /**
   * @function isError
   * @param suspect Suspect for checking
   */
  let isError = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/Error/i);
  };

  /**
   * @function isRegExp
   * @param suspect Suspect for checking
   */
  let isRegExp = (suspect: any): boolean => {
    return suspect instanceof RegExp;
  };

  /**
   * @function isDate
   * @param suspect Suspect for checking
   */
  let isDate = (suspect: any): boolean => {
    return suspect instanceof Date;
  };

  /**
   * @function isBoolean
   * @param suspect Suspect for checking
   */
  let isBoolean = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/false/i);
  };

  /**
   * @function isFunction
   * @param suspect Suspect for checking
   */
  let isFunction = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/function/i);
  };

  /**
   * @function isString
   * @param suspect Suspect for checking
   */
  let isString = (suspect: any): boolean => {
    return Object.getPrototypeOf(suspect) === Object.getPrototypeOf('');
  };

  /**
   * @function isElement
   * @param suspect Suspect for checking
   */
  let isElement = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/HTML.*Element/i);
  };

  /**
   * @function isFinite
   * @param suspect Suspect for checking
   */
  let isFinite = (suspect: any): boolean => {
    return Number.isFinite(suspect);
  };

  /**
   * @function isNumber
   * @param suspect Suspect for checking
   */
  let isNumber = (suspect: any): boolean => {
    return !!safeGetPrototypeString(suspect).match(/0/i);
  };

  /**
   * @function isSymbol
   * @param suspect Suspect for checking
   */
  let isSymbol = (suspect: any): boolean => {
    return !!(typeof suspect).match(/symbol/i);
  };

  // TBD: Add isType tests 
  let isType = {
    isUndefined, isArray, isObject,
    isInteger, isNumberLike, isNonNegativeInteger,
    isNull, isNaN, isWeakSet, isSet,
    isWeakMap, isMap, isError,
    isRegExp, isDate, isBoolean,
    isFunction, isString, isElement,
    isFinite, isNumber, isSymbol
  };

  // --- Features
  /**
   * @function aPush
   * @param array   Original array
   * @param element Elements to push
   *
   * TBD: Need Unit Tests
   */
  let aPush = (array: any[], element: any | any[]): any[] => {
    if (callback(isUndefined, element)) return array || [];
    if (callback(isArray, element)) {
      return [...(array || []), ...element];
    } else {
      return [...(array || []), element];
    }
  };

  /**
   * @function oPush
   * @param object  Original object
   * @param element Elements to push
   *
   * TBD: Need Unit Tests
   */
  let oPush = (object: object | undefined, element: any): object => {
    if (callback(isUndefined, element)) return object || {};
    if (callback(isObject, element)) {
      return {
        ...(object || {}),
        ...element
      };
    } else {
      return {
        ...(object || {}),
        element
      };
    }
  };

  /**
   * @name tcl.push
   * @function push
   * @param origin  Original object
   * @param element Elements to push
   *
   * TBD: Need Unit Tests
   */
  let push = (origin: any, element: any): any => {
    if (callback(isUndefined, origin)) {
      return null;
    } else if (callback(isArray, origin)) {
      return callback(aPush, origin, element);
    } else {
      return callback(oPush, origin, element);
    }
  };

  /**
   * @function aPop
   * @param array Original array
   * @param count Number of elements to pop
   *
   * TBD: Need Unit Tests
   */
  let aPop = (array: any, count: number = 1): any[] => {
    if (array.length <= count) {
      return [];
    } else {
      let _return = [];
      for (let i = 0; i < array.length - count; i++) {
        _return.push(array[i]);
      }
      return _return;
    }
  };

  /**
   * @name tcl.first
   * @function aFirst
   * @param array Original Array
   * @param count Number of elements to obtain
   *
   * TBD: [Bug] Should be able to process string since it is also array like
   */
  let aFirst = (array: any[], count: number): any[] | any => {
    if (callback(isArray, array) && callback(isNonNegativeInteger, count)) {
      if (count === 1) {
        return [array[0]] || null;
      } else {
        let _return = [];
        for (let i = 0; i < count && i < array.length; i++) {
          _return.push(array[i]);
        }
        return _return;
      }
    } else {
      if (callback(isUndefined, count)) {
        return array[0] || null;
      }
      return null;
    }
  };

  /**
   * @name tcl.reverse
   * @function aReverse
   * @param array Original Array
   */
  let aReverse = (array: any[]): any[] => {
    if (callback(isArray, array)) {
      let _result = [];
      for (let i = 1; i <= array.length; i++) {
        _result.push(array[array.length - i]);
      }
      return _result;
    } else {
      return array;
    }
  };

  /**
   * @name tcl.initial
   * @function aInitial
   * @param array Original Array
   * @param count Number of elements to remove
   */
  let aInitial = (array: any[], count: number = 1): any[] => {
    if (callback(isArray, array) && callback(isNonNegativeInteger, count)) {
      let _result = [];
      for (let i = 0; i < array.length - count; i++) {
        _result.push(array[i]);
      }
      return _result;
    } else {
      return array;
    }
  };

  /**
   * @name tcl.last
   * @function aLast
   * @param array Original Array
   * @param count Number of elements to obtain from the tail
   */
  let aLast = (array: any[], count: number = 1): any[] => {
    if (callback(isArray, array) && callback(isNonNegativeInteger, count)) {
      return callback(
        aReverse,
        callback(aFirst, callback(aReverse, array), count)
      );
    } else {
      return array;
    }
  };

  /**
   * @name tcl.rest
   * @function aRest
   * @param array Original Array
   * @param count Element index of elements to obtain
   */
  let aRest = (array: any[], index: number = 1): any[] => {
    if (callback(isArray, array) && callback(isNonNegativeInteger, index)) {
      let _result = [];
      for (let i = index; i < array.length; i++) {
        _result.push(array[i]);
      }
      return _result;
    } else {
      return array;
    }
  };

  /**
   * @name tcl.flatten
   * @function aFlatten
   * @param array Array for flattening
   * 
   * TBD: add shallow function
   */
  let aFlatten = (array: any, shallow: boolean = false): any[] => {
    if (callback(isArray, array)) {
      let _result = Array(...array);
      for (let i = 0; i < _result.length; i++) {
        if (callback(isArray, _result[i])) {
          _result.splice(i, 1, ..._result[i]);
          i--;
        }
      }
      return _result;
    } else {
      return array;
    }
  };

  /**
   * @name tcl.without
   * @function aWithout
   * @param array Array for filtering
   * @param values Elements to filter
   */
  let aWithout = (array: any, ...values: any[]): any[] => {
    if (callback(isArray, array) && values.length > 0) {
      let _result = [];
      for (let i = 0; i < array.length; i++) {
        if ((values.indexOf(array[i]) === -1)) {
          _result.push(array[i]);
        }
      }
      return _result;
    } else {
      return array;
    }
  };

  /**
   * @name tcl.compact
   * @function aCompact
   * @param array Array to remove falsy values
   */
  let aCompact = (array: any): any[] => {
    if (callback(isArray, array)) {
      let _result = Array(...array);
      for (let i = 0; i < _result.length; i++) {
        if (!_result[i]) {
          _result.splice(i, 1);
        }
      }
      return _result;
    } else {
      return array;
    }
  };

  /**
   * @name tcl.union
   * @function aUnion
   * @param arrayList 
   */
  let aUnion = (...arrayList: any[][]): any[] => {
    let _result = [];
    for (let i = 0; i < arrayList.length; i++) {
      if (!callback(isArray, arrayList[i])) {
        _result.push(arrayList[i]);
        continue;
      }
      for (let j = 0; j < arrayList[i].length; j++) {
        if (_result.indexOf(arrayList[i][j]) === -1) {
          _result.push(arrayList[i][j]);
        }
      }
    }
    return _result;
  };

  /**
   * @name tcl.intersection
   * @function aIntersection
   * @param arrayList Arrays for obtaining intersections
   */
  let aIntersection = (...arrayList: any[][]): any[] => {
    if (arrayList.length <= 1 || !callback(isArray, arrayList[0])) {
      return arrayList || [];
    } else {
      let _result = [];
      for (let i = 0; i < arrayList[0].length; i++) {
        let _apperance = 0;
        for (let j = 0; j < arrayList.length; j++) {
          if (arrayList[j].indexOf(arrayList[0][i]) > -1) {
            _apperance++;
          }
        }
        if (_apperance === arrayList.length) {
          _result.push(arrayList[0][i]);
        }
      }
      return _result;
    }
  };

  /**
   * @name tcl.difference
   * @function aDifference
   * @param originArray      Original Array
   * @param comparisionArray Comparision Array
   * 
   * @description This function works as the math form: A/B for sets A and B
   */
  let aDifference = (originArray: any[], comparisionArray: any[]): any => {
    if (!callback(isArray, originArray) || !callback(isArray, comparisionArray)) {
      return originArray;
    } else {
      let _result = [];
      for (let i = 0; i < originArray.length; i++) {
        if (comparisionArray.indexOf(originArray[i]) === -1) {
          _result.push(originArray[i]);
        }
      }
      return _result;
    }
  };

  /**
   * @name tcl.aRange
   * @function range
   * @param nArg1 start, will be end if there only exists 1 argument
   * @param nArg2 end, optional
   * @param nArg3 step, optional
   */
  let aRange = (nArg1?: number, nArg2?: number, nArg3?: number): number[] => {
    let _result: number[] = [];
    // TBD: Workaround typescript checks
    let start = 0;
    let end = 0;
    let step = 1;

    if (!callback(isNumberLike, nArg2) && callback(isNumberLike, nArg1)) {
      Number(nArg1) > 0 ? end = Number(nArg1) : start = Number(nArg1);
    }
    else if (callback(isNumberLike, nArg1) && callback(isNumberLike, nArg2)) {
      start = nArg1; end = nArg2;

      if (callback(isNumberLike, nArg3)) {
        step = nArg3;
      } else {
        if (start > end) {
          step = -1;
        }
      }
    } else {
      return _result;
    }

    let k = 0;
    while ((step) * (start + k * step - end) < 0) {
      _result.push(start + k * step);
      k++;
    }
    return _result;
  };

  /**
   * @name tcl.chunk
   * @function aChunk
   * @param array Array to modify
   * @param length Target chunk length
   */
  let aChunk = (array: any[], length: number = 0): any[][] => {
    if (!callback(isNonNegativeInteger, length) || !length) {
      return [array];
    } else if (callback(isArray, array) && length < array.length) {
      let _result = [];
      let _array: any[] = [];
      for (let i = 0; i < array.length; i++) {
        if (i % length === 0) {
          _result.push([]);
          _array = _result[_result.length - 1];
        }
        _array.push(array[i]);
      }
      return _result;
    } else {
      return [array];
    }
  };

  /**
   * @name tcl.object
   * @function aObject
   * @param keys Keys of target object
   * @param values Values of target object
   */
  let aObject = (keys: any[] | any, values: any[]): object => {
    let _return = {};
    if (!callback(isArray, keys) && !callback(isArray, keys)) {
      _return[keys] = values;
    } else {
      for (let i = 0; i < keys.length && i < values.length; i++) {
        _return[keys[i]] = values[i];
      }
    }
    return _return;
  };

  /**
   * @name tcl.keys
   * @function oKeys
   * @param obj Object
   * 
   * @ignore unit tests
   */
  let oKeys = (obj: object): any[] => {
    return Object.getOwnPropertyNames(obj);
  };

  /**
   * @name tcl.allKeys
   * @function oAllKeys
   * @param obj Object
   * 
   * @ignore unit tests   * 
   */
  let oAllKeys = (obj: object): any[] => {
    let _result = [];
    // tslint:disable-next-line
    for (let _prop in obj) {
      _result.push(_prop);
    }
    return _result;
  };

  /**
   * @name tcl.values
   * @function oValues
   * @param obj Object
   * 
   * @ignore unit tests
   */
  let oValues = (obj: object): any[] => {
    let _result = callback(oKeys, obj);
    for (let i = 0; i < _result.length; i++) {
      _result[i] = obj[_result[i]];
    }
    return _result;
  };

  // --- Assign tcl with all function signatures
  Object.assign(tcl, {
    push,
    pop: aPop, // TBD: Consider oPop for objects

    // Array Functions
    first: aFirst, reverse: aReverse, initial: aInitial,
    last: aLast, rest: aRest, flatten: aFlatten,
    compact: aCompact, without: aWithout, union: aUnion,
    intersection: aIntersection, difference: aDifference,
    range: aRange, chunk: aChunk, object: aObject,

    // Object Functions
    keys: oKeys, allKeys: oAllKeys, values: oValues,

    // Aux Functions
    isType
  });

  Object.assign(tcl, {
    self: tcl
  });
  // ---

  // TBD: Test on more versions
  // Works on Node.js 9.9
  Object.assign(oRoot, { tcl });
  exports = module.exports = tcl;
  // ---
})();
